import com.xbcx.MainController;
import com.xbcx.entity.User;
import com.xbcx.exception.CustomerException;
import com.xbcx.mapper.UserMapper;
import com.xbcx.service.UserService;
import com.xbcx.util.AESUtil;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.Resource;
import javax.crypto.spec.SecretKeySpec;
import java.security.Key;

/**
 * @Author: 伍炳清
 * @Date: 2021/3/23 10:25
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = MainController.class)
public class TestAES {


    @Test
    public void encrypt() {

        String s = AESUtil.jaAES("乱码", "wbq");
        System.out.println(s);

    }
    @Test
    public void decrypt() {

       String s = AESUtil.jieAES("hYK1NiJuu8wo2QI+vxOTWB==", "wbq");
        System.out.println(s);

    }

    @Test
    public void Test() {
        int x = 3, y = 2, z = 5;

        System.out.println(y += z-- / ++x);
    }


}
