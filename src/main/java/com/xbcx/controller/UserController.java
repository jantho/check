package com.xbcx.controller;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.xbcx.entity.User;
import com.xbcx.exception.CustomerException;
import com.xbcx.mapper.UserMapper;
import com.xbcx.model.UserListDTO;
import com.xbcx.model.UserListVO;
import com.xbcx.service.UserService;
import com.xbcx.util.JsonResult;
import com.xbcx.util.LayuiReplay;
import org.springframework.web.bind.annotation.*;
import javax.annotation.Resource;
import java.util.Map;

/**
 * @Author: 伍炳清
 * @Date: 2021/3/23 15:52
 */
@RestController
@RequestMapping("/User")
@CrossOrigin
public class UserController {
    @Resource
    private UserService userService;
    @Resource
    private UserMapper userMapper;
    JsonResult jsonResult = new JsonResult();

    @RequestMapping("/addUser")
    public Map<String, Object> add(@RequestBody User user) throws CustomerException {
        userService.addUser(user);
        jsonResult.setCode("1");
        jsonResult.setMsg("添加成功");
        return jsonResult.getValues();
    }

    @RequestMapping("/deleteUser")
    public Map<String, Object> delete(@RequestParam Integer id, @RequestParam Integer del) throws CustomerException {
        User user = userMapper.selectById(id);
        user.setIsDel(del);
        userService.updateUser(user);
        jsonResult.setCode("1");
        jsonResult.setMsg("删除成功");
        return jsonResult.getValues();
    }

    @RequestMapping("/pageUser")
    @CrossOrigin
    public LayuiReplay<UserListVO> findALl(@RequestParam Integer pageNo, @RequestParam Integer pageSize) throws CustomerException {
        UserListDTO dto = new UserListDTO(pageNo,pageSize);
        Page<UserListVO> pageUser = userService.pageUser(dto);
        return new LayuiReplay<>(0, "查询成功", (int) pageUser.getTotal(), pageUser.getRecords());
    }

    @RequestMapping("/login")
    public Map<String, Object> login(@RequestParam String account, @RequestParam String pwd) throws CustomerException {
        User login = userService.login(account, pwd);
        jsonResult.setData(login);
        jsonResult.setCode("1");
        jsonResult.setMsg("欢迎" + login.getName());
        return jsonResult.getValues();
    }

}
