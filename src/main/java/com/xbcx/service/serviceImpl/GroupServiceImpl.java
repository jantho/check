package com.xbcx.service.serviceImpl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.xbcx.entity.Classes;
import com.xbcx.entity.Group;
import com.xbcx.entity.GroupUser;
import com.xbcx.entity.User;
import com.xbcx.exception.CustomerException;
import com.xbcx.mapper.ClassesMapper;
import com.xbcx.mapper.GroupMapper;
import com.xbcx.mapper.GroupUserMapper;
import com.xbcx.mapper.UserMapper;
import com.xbcx.model.GroupListVO;
import com.xbcx.model.PageDTO;
import com.xbcx.model.UserListDTO;
import com.xbcx.model.UserListVO;
import com.xbcx.service.GroupService;
import com.xbcx.service.UserService;
import com.xbcx.util.Md5Utils;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Date;
import java.util.List;

/**
 * @author 伍炳清
 * @date 2021-10-16 14:50
 */

@Service("GroupService")
public class GroupServiceImpl implements GroupService {

    @Resource
    private GroupMapper groupMapper;
    @Resource
    private UserMapper userMapper;
    @Resource
    private GroupUserMapper groupUserMapper;
    @Resource
    private ClassesMapper classesMapper;


    @Override
    public void addGroup(Group group) throws CustomerException {

        //判空
        if (group.getBeginTime() == null) {
            throw new CustomerException("起始时间不能为空！");
        }
        if (group.getTraitId() == null) {
            throw new CustomerException("考勤组特征id不能为空！");
        }
        if (group.getComId() == null) {
            throw new CustomerException("创建公司id不能为空！");
        }
        if (group.getUid() == null) {
            throw new CustomerException("创建人id不能为空！");
        }
        group.setCreateTime(new Date());
        group.setUpdateTime(new Date());
        group.setIsDel(0);
        groupMapper.insert(group);


    }

    @Override
    public Page<GroupListVO> pageGroup(PageDTO dto) throws CustomerException {
        //判空
        if (dto.getPageNo() == null) {
            throw new CustomerException("当前页数不能为空！");
        }
        if (dto.getPageSize() == null) {
            throw new CustomerException("分页大小不能为空！");
        }
        Page<GroupListVO> page = new Page<>(dto.getPageNo(), dto.getPageSize());
        Page<GroupListVO> groupList = groupMapper.getGroupList(page, dto);
        //加入考勤组人员名集合
        List<GroupListVO> groupListVOs = groupList.getRecords();

        for (GroupListVO group : groupListVOs) {

            LambdaQueryWrapper<GroupUser> wrapper = new LambdaQueryWrapper<>();
            wrapper.eq(GroupUser::getGroupId, group.getId());
            //得到user的id集合groupUsers
            List<GroupUser> groupUsers = groupUserMapper.selectList(wrapper);
            StringBuilder userNames = new StringBuilder();

            for (GroupUser groupUser : groupUsers) {

                User user = userMapper.selectById(groupUser.getUserId());

                if (userNames.length() == 0) {
                    userNames.append(user.getName());
                } else {
                    userNames.append("," + user.getName());
                }
            }
            //添加好了第一个考勤组的人员名单
            group.setAllUserName(userNames.toString());
        }
        //添加好了所有考勤组的人员名单
        groupList.setRecords(groupListVOs);
        return groupList;
    }

    @Override
    public void addClasses(Classes classes) throws CustomerException {
        //判空
        if (classes.getGroupId() == null) {
            throw new CustomerException("考勤组id不能为空！");
        }
        if (classes.getBeginTime() == null) {
            throw new CustomerException("起始时间不能为空！");
        }
        if (classes.getEndTime() == null) {
            throw new CustomerException("结束时间不能为空！");
        }
        if (classes.getComId() == null) {
            throw new CustomerException("创建公司id不能为空！");
        }
        if (classes.getUid() == null) {
            throw new CustomerException("创建人id不能为空！");
        }
        classes.setCreateTime(new Date());
        classes.setIsDel(0);
        classesMapper.insert(classes);

    }


}

