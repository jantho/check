package com.xbcx.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Date;


/**
 * @Author: 伍炳清
 * @Date: 2021/3/23 11:53
 */
@Data
@NoArgsConstructor
@AllArgsConstructor

//定义表名
@TableName(value = "t_groups")
public class Group extends Model<Group> implements Serializable {

    private static final long serialVersionUID = 1L;
    /**
     * 考勤组主键
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;
    /**
     * 考勤组名
     */
    @TableField("name")
    private String name;
    /**
     * 考勤组开始时间
     */
    @TableField("begin_time")
    //Timestamp
    private Date beginTime;

    /**
     * 考勤组特征id
     */
    @TableField("trait_id")
    private Integer traitId;
    /**
     * 创建公司id
     */
    @TableField("com_id")
    private Integer comId;
    /**
     * 创建人id
     */
    @TableField("uid")
    private Integer uid;
    /**
     * 考勤组创建时间
     */
    @TableField("create_time")
    //Timestamp
    private Date createTime;
    /**
     * 考勤组更新时间
     */
    @TableField("update_time")
    private Date updateTime;
    /**
     * 考勤组是否被删除(0:未删除 1：已删除)
     */
    @TableField("is_del")
    private Integer isDel;


}
