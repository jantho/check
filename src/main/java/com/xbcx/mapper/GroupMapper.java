package com.xbcx.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.xbcx.entity.Group;
import com.xbcx.entity.User;
import com.xbcx.model.GroupListVO;
import com.xbcx.model.PageDTO;
import com.xbcx.model.UserListDTO;
import com.xbcx.model.UserListVO;
import org.apache.ibatis.annotations.Param;

/**
 * @author 伍炳清
 * @date 2020-10-15 20:30
 */
//接口只需继承Mapper
public interface GroupMapper extends BaseMapper<Group> {
    /**
     * 分页查询考勤组列表
     *
     * @param page
     * @return
     */
    Page<GroupListVO> getGroupList(Page<GroupListVO> page, @Param("dto") PageDTO dto);

}
