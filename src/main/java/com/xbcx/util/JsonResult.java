package com.xbcx.util;

import java.util.HashMap;
import java.util.Map;

// 通过内置的Map , 添加指定的信息和拓展的信息
// 指定的信息:  code / msg / data
// 拓展的信息: put(key,value)
// 得到 values
public class JsonResult {
    private Map<String, Object> values = null;

    {
        values = new HashMap<String, Object>();
    }

    public void setCode(String code) {
        values.put("code", code);
    }

    public void setMsg(String msg) {
        values.put("msg", msg);
    }

    public void setData(Object data) {
        values.put("data", data);
    }

    public void setToken(Object token) {
        values.put("token", token);
    }

    public void put(String key, Object value) {
        values.put(key, value);
    }

    public Map<String, Object> getValues() {
        return values;
    }
}
