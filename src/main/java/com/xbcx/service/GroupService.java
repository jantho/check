package com.xbcx.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.xbcx.entity.Classes;
import com.xbcx.entity.Group;
import com.xbcx.entity.User;
import com.xbcx.exception.CustomerException;
import com.xbcx.model.GroupListVO;
import com.xbcx.model.PageDTO;
import com.xbcx.model.UserListDTO;
import com.xbcx.model.UserListVO;

/**
 * @author 伍炳清
 * @date 2020-10-16 14:50
 */
public interface GroupService {
    /**
     * 新增考勤组
     *
     * @param group 考勤组
     */

    void addGroup(Group group) throws CustomerException;

    /**
     * 考勤组分页列表
     *
     * @param dto user分页信息
     * @return
     * @throws CustomerException
     */
    Page<GroupListVO> pageGroup(PageDTO dto) throws CustomerException;

    /**
     * 新增考勤组班次
     *
     * @param classes 班次
     * @throws CustomerException
     */
    void addClasses(Classes classes) throws CustomerException;

}
