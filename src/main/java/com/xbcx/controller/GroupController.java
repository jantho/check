package com.xbcx.controller;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.xbcx.entity.Classes;
import com.xbcx.entity.Group;
import com.xbcx.entity.GroupUser;
import com.xbcx.entity.User;
import com.xbcx.exception.CustomerException;
import com.xbcx.mapper.ClassesMapper;
import com.xbcx.mapper.GroupMapper;
import com.xbcx.mapper.UserMapper;
import com.xbcx.model.GroupListVO;
import com.xbcx.model.PageDTO;
import com.xbcx.model.UserListDTO;
import com.xbcx.model.UserListVO;
import com.xbcx.service.GroupService;
import com.xbcx.service.UserService;
import com.xbcx.util.JsonResult;
import com.xbcx.util.LayuiReplay;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.Map;

/**
 * @Author: 伍炳清
 * @Date: 2021/3/23 15:52
 */
@RestController
@RequestMapping("/Group")
@CrossOrigin
public class GroupController {
    @Resource
    private GroupService groupService;



    JsonResult jsonResult = new JsonResult();
    @RequestMapping("/addGroup")
    @CrossOrigin
    public  Map<String, Object> addGroup(@RequestBody Group group) throws CustomerException {
        groupService.addGroup(group);
        jsonResult.setCode("1");
        jsonResult.setMsg("添加成功");
        return jsonResult.getValues();
    }

    @RequestMapping("/pageGroup")
    @CrossOrigin
    public LayuiReplay<GroupListVO> pageGroup(@RequestParam Integer pageNo, @RequestParam Integer pageSize) throws CustomerException {
        PageDTO dto = new PageDTO(pageNo, pageSize);
        Page<GroupListVO> pageGroup = groupService.pageGroup(dto);
        return new LayuiReplay<>(0, "查询成功", (int) pageGroup.getTotal(), pageGroup.getRecords());
    }

    @RequestMapping("/addClasses")
    @CrossOrigin
    public  Map<String, Object> addClasses(@RequestBody Classes classes) throws CustomerException {
        groupService.addClasses(classes);
        jsonResult.setCode("1");
        jsonResult.setMsg("添加成功");
        return jsonResult.getValues();
    }


}
