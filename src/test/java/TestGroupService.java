import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.xbcx.MainController;
import com.xbcx.exception.CustomerException;
import com.xbcx.mapper.GroupMapper;
import com.xbcx.model.GroupListVO;
import com.xbcx.model.PageDTO;
import com.xbcx.service.GroupService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.Resource;
import java.util.Date;

/**
 * @Author: 伍炳清
 * @Date: 2021/3/23 10:25
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = MainController.class)
public class TestGroupService {

    @Resource
    private GroupService groupService;


    @Test
    public void addGroup() {

    }

    @Test
    public void page() {
        PageDTO page = new PageDTO(1, 3);
        try {
            Page<GroupListVO>   groupListVOPage = groupService.pageGroup(page);
            System.out.println(groupListVOPage.getRecords());
        } catch (CustomerException e) {
            System.out.println(e.getMessage());
        }


    }

    @Test
    public void Test() {
        int createTime = (int) new Date().getTime();
        System.out.println(new Date().getTime());
        System.out.println("转int:" + createTime);
        System.out.println("转回:" + Long.valueOf(createTime));


    }


}
