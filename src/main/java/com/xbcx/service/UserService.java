package com.xbcx.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.xbcx.exception.CustomerException;
import com.xbcx.entity.User;
import com.xbcx.model.UserListDTO;
import com.xbcx.model.UserListVO;

/**
 * @author 伍炳清
 * @date 2020-10-16 14:50
 */
public interface UserService {
    /**
     * 新增user
     *
     * @param user 用户
     */

    void addUser(User user) throws CustomerException;

    /**
     * xiugaiuser
     *
     * @param user 用户
     */
    void updateUser(User user) throws CustomerException;

    /**
     * @param account 用户名
     * @param pwd     密码
     * @throws CustomerException
     */
    User login(String account, String pwd) throws CustomerException;

    /**
     * 用户分页列表
     * @param dto user分页信息
     * @return
     * @throws CustomerException
     */
    Page<UserListVO> pageUser(UserListDTO dto) throws CustomerException;

}
