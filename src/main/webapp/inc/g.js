var config = {
  root: "http://localhost:8088/portal", //本地
  // root: "http://192.168.110.110:8088/portal", //本地测试
  // root: "https://delivery.xianxiaochu.cn/portal", //生产环境/
  ossroot: "https://xxw-xxcpro.oss-cn-chengdu.aliyuncs.com/",
  pageSize: 10,
  base: "/",
};

var AJAX = new (function () {
  var _xhr,
    K = "Authorization",
    abs = null,
    t = this,
    tk = null;

  function finish(v, cb) {
    if (cb == null) return;
    var o;
    if (v && v.length > 1) {
      try {
        o = JSON.parse(v);
      } catch (e) {
        o = v;
      }
    }
    if (o && o.code < 0) {
      Comm.message(o.msg);
    }
    if (o && o.code == 110) {
      Comm.msg("登录已过期，请重新登录", 5);
      setTimeout(function () {
        AJAX.clearTag();
        if (self == top) {
          top.Comm.go(config.base + "login.html");
        } else {
          top.Comm.go(config.base + "login.html");
        }
      }, 2000);
      Comm.loading(false);
      return;
    }
    cb(o);
    setTimeout(function () {
      Comm.loading(false);
    }, 500);
  }

  function ab() {
    if (abs == null) {
      abs = top.Comm.db(K);
      if (abs == null) abs = "";
    }
    return abs;
  }

  function repair(api) {
    // console.log(api);
    var a = ab();
    if (true) {
      api += (api.indexOf("?") > 0 ? "&" : "?") + "&timespan=" + Math.random();
      tk = a;
    }
    return api;
  }

  function deobj(obj) {
    if (obj == null) return "";
    var s = [];
    for (var i in obj) {
      s.push(i + "=" + encodeURIComponent(obj[i]));
    }
    return s.join("&");
  }

  function error(code, cb) {
    Comm.loading(false);
    cb &&
      cb({
        code: -1,
        msg: "服务器异常",
      });
  }

  function init(post, url, data, cb, asyn, isJson, isLoading) {
    if (isLoading == null) isLoading = true;
    if (isLoading == true) Comm.loading(true);
    //  console.log(post+"aaaaaaaaaaaaaa")
    url = t.Uri() + repair(url);
    if (asyn == null) asyn = true;
    if (isJson == null) isJson = false;
    //	console.log("isJSON IS"+isJson);
    var xhr = new XMLHttpRequest();
    xhr.onreadystatechange = function () {
      if (this.readyState == 4) {
        if (this.status == 200) {
          finish(this.responseText, cb);
        } else {
          error(this.status, cb);
        }
      }
    };
    xhr.open(post ? "POST" : "GET", url, asyn);

    if (tk != null && tk != "") {
      xhr.setRequestHeader(K, tk);
    } else {
      xhr.setRequestHeader(K, Comm.db(K));
    }

    if (post && isJson) {
      data = JSON.stringify(data);
      xhr.setRequestHeader("Content-Type", "application/json");
    } else if (post) {
      data = deobj(data);
      xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
    }
    xhr.send(data);
  }

  /*----AJAX公用方法-----*/

  /*获取服务器接口根路径*/
  t.Uri = function () {
    return window["config"] && window["config"]["root"] ? config.root : "";
  };

  /*获取服务器调试页面根路径*/
  t.WebRoot = function () {
    return window["config"] && window["config"]["webroot"] ?
      config.webroot :
      "";
  };

  /*调用用户登录token*/
  t.setTag = function (a) {
    abs = a;
    Comm.db(K, abs);
  };

  /*用户登录成功*/
  t.setLogin = function (d) {
    var u = d.data.customerInfo;
    var s = d.data.storeInfo;
    AJAX.setTag("Bearer " + u.token);
    Comm.db("_userinfo", {
      id: u.userId,
      p: u.phone,
      status: u.status,
      headImg: s.companyLogo,
      nickname: u.nickname,
      realname: u.realname,
      phone: s.phone,
    });
  };

  /*退出登录清除token*/
  t.clearTag = function () {
    Comm.db(K, null);
    Comm.db("_userinfo", null);
  };

  /*执行GET方法，一般用于从服务器获取数据，api长度尽量不超过1000字节*/
  t.GET = function (api, cb, asyn, isJson, isLoading) {
    init(false, api, null, cb, asyn, false, isLoading);
  };
  /*执行POST方法，一般用于向服务器提交数据，data建议不为空*/
  t.POST = function (api, data, cb, isJson, isLoading) {
    init(true, api, data, cb, true, isJson, isLoading);
  };
  /*根据用户凭证判断用户是否登录*/
  t.isLogin = function () {
    return ab().length > 0;
  };
})();
var Comm = {
  Cookie: {
    set: function (name, value) {
      document.cookie = name + "=" + escape(value);
    },
    get: function (name) {
      var arr,
        reg = new RegExp("(^| )" + name + "=([^;]*)(;|$)");
      if ((arr = document.cookie.match(reg))) return unescape(arr[2]);
      else return "";
    },
  },
  GetOpData: function (data, dname, vname, isPrice, isTime, isMap) {
    var darr = [];
    var varr = [];
    var dmap = [];
    if (isPrice == null) isPrice = false;
    if (isTime == null) isTime = false;
    if (isMap == null) isMap = false;
    data.forEach(function (item, index) {
      if (isPrice) {
        varr[index] = Comm.price(item[vname]);
      } else if (isTime) {
        varr[index] = Comm.dateTimes(item[vname]);
      } else {
        varr[index] = item[vname];
      }
      darr[index] = item[dname];
      if (isMap) {
        var m = {
          name: darr[index],
          value: varr[index],
        };
        dmap.push(m);
      }
    });
    if (isMap)
      return {
        name: darr,
        data: dmap,
      };
    return {
      name: darr,
      data: varr,
    };
  },

  resultCode: function (code) {
    return code == 1;
  },

  checkRole: function (roleId) {
    var staff = Comm.db("user");
    var list = staff.storeList;
    for (var i in list) {
      if (list[i].roleId == roleId || list[i].roleId == 1) {
        return true;
      }
    }
    return false;
  },

  checkCode: function (code, itemId, gosId) {
    var str = itemId == 1 ? "商品" : "规格";
    if (Comm.checkEmpty(code, "未填写" + str + "编码")) return;
    if (code.length != 18) {
      Comm.msg(
        str + "编码长度错误<br>需要长度：18<br>当前长度：" + code.length,
        5
      );
      return;
    }
    AJAX.GET(
      "/admin/goods/checkCode?gosId=" +
      gosId +
      "&itemId=" +
      itemId +
      "&item=" +
      code,
      function (d) {
        if (d.code != 1) {
          layer.closeAll();
          Comm.msg(d.msg, 5);
        } else if (d.code == 1) {
          Comm.msg("编码可用", 1);
        }
      }
    );
  },

  mainStockLoad: function (selectId, id) {
    AJAX.GET('/admin/mainstock/getMainStockSimpleList', function (d) {
      if (d.code == 1 && d.data.length > 0) {
        var list = d.data;
        $('#' + selectId).html('');
        $('#' + selectId).append("<option value=''></option>");
        for (var i in list) {
          if (list[i].mainStockId == id) {
            $('#' + selectId).append("<option value = '" + list[i].mainStockId + "' selected>" + list[i]
              .stockName + "</option>")
          } else {
            $('#' + selectId).append("<option value = '" + list[i].mainStockId + "'>" + list[i]
              .stockName + "</option>")
          }
        }
        Comm.form.render();
      }
    })
  },

  categoryOneLoad: function (selectId) {
    $('#' + selectId).html("");
    $('#' + selectId).append("<option value = ''>请选择</option>");
    AJAX.GET('/admin/dict/GetListByParentCode?dictCode=categoryDict', function (d) {
      if (d.code == 1) {
        for (var i in d.data) {
          $('#' + selectId).append("<option value=" + d.data[i].dictid + ">" + d.data[i].dictname + "</option>");
        }
      }
      Comm.form.render("select");
    });
  },

  getWarehouseId: function () {
    var warehouseId = Comm.query("warehouseId");
    if (warehouseId > 0) {
      return warehouseId;
    } else {
      return Comm.db("user").warehouseId;
    }
  },

  categoryTwoLoad: function (selectId, pid) {
    AJAX.GET('/admin/dict/GetListByDictPid/' + pid, function (d) {
      $('#' + selectId).html("");
      $('#' + selectId).append("<option value=''>请选择</option>");
      if (d.code == 1) {
        for (var i in d.data) {
          $('#' + selectId).append("<option value=" + d.data[i].dictid + ">" + d.data[i].dictname + "</option>");
        }
      }
      Comm.form.render();
    });
  },

  enterListen: function () {
    document.onkeydown = function (e) {
      var ev = document.all ? window.event : e;
      if (ev.keyCode == 13) {
        Comm.search();
      }
    };
  },

  checkPrice: function (price, type, msg) {
    var reg = /^\d+$/; //非负整数
    var reg2 = /^\+?(\d*\.\d{2})$/; //两位小数
    var reg3 = /^\d*\.{0,1}\d{0,1}$/; //一位小数
    var reg4 = /(^[1-9](\d+)?(\.\d{1,2})?$)|(^[1-9]$)|(^\d\.[1-9]{1,2}$)|(^\d\.[0]{1}[1-9]{1}$|(^\d\.[1-9]{1}[0]{1}$)$)/;
    if (type == 1) {
      //非负整数
      if (!reg.test(price)) {
        Comm.msg(msg, 5);
        return true;
      }
    } else if (type == 2) {
      //非负两位小数
      if (!reg.test(price) && !reg2.test(price) && !reg3.test(price)) {
        Comm.msg(msg, 5);
        return true;
      }
    } else if (type == 3) {
      //非负一位小数
      if (!reg.test(price) && !reg3.test(price)) {
        Comm.msg(msg, 5);
        return true;
      }
    }
  },

  getSearchTime: function (type) {
    var temp = new Date().getTime() - 24 * 60 * 60 * 1000;
    var sDate = new Date(temp);
    var sYear = sDate.getFullYear();
    var sMonth = sDate.getMonth() + 1;
    var sday = sDate.getDate();
    if (sMonth < 10) {
      sMonth = "0" + sMonth;
    }
    if (sday < 10) {
      sday = "0" + sday;
    }
    var startDate = sYear + "-" + sMonth + "-" + sday + " 20:00:00";


    var eDate = new Date();
    var eYear = eDate.getFullYear();
    var eMonth = eDate.getMonth() + 1;
    var eday = eDate.getDate();
    if (eMonth < 10) {
      eMonth = "0" + eMonth;
    }
    if (eday < 10) {
      eday = "0" + eday;
    }
    var endDate = eYear + "-" + eMonth + "-" + eday + " 19:59:59";
    var returnDate = [];
    returnDate.push(startDate);
    returnDate.push(endDate);
    return returnDate;
  },

  getTomorrow: function () {
    var timestamp = (new Date()).getTime() + 24 * 60 * 60 * 1000;
    var date = new Date(timestamp);
    var year = date.getFullYear();
    var month = date.getMonth() + 1;
    var day = date.getDate();
    if (month < 10) {
      month = "0" + month;
    }
    if (day < 10) {
      day = "0" + day;
    }
    return year + "-" + month + "-" + day;
  },

  checkEmptyNoMsg: function (text) {
    var reg = /^[ ]*$/;
    if (typeof text == "undefined" || reg.test(text)) {
      return true;
    } else {
      return false;
    }
  },

  timeLoad: function (start, end, startId, endId) {
    var list = [];
    if (this.checkEmptyNoMsg(start) && this.checkEmptyNoMsg(end)) {
      start = this.getSearchTime()[0];
      end = this.getSearchTime()[1];
      layui.use("laydate", function () {
        var laydate = layui.laydate;
        laydate.render({
          elem: "#" + startId,
          trigger: "click",
          type: "datetime",
          // value: start,
        });
        laydate.render({
          elem: "#" + endId,
          trigger: "click",
          type: "datetime",
          // value: end,
        });
      });
    }
    list.push(start);
    list.push(end);
    return list;
  },

  checkEmpty: function (text, msg) {
    var reg = /^[ ]*$/;
    if (typeof text == "undefined" || reg.test(text)) {
      Comm.msg(msg, 5);
      return true;
    }
  },

  htmlDecode: function (text) {
    //1.首先动态创建一个容器标签元素，如DIV
    // var temp = document.createElement("div");
    // //2.然后将要转换的字符串设置为这个元素的innerHTML
    // temp.innerHTML = text;
    // var output = temp.innerText || temp.textContent;
    // temp = null;
    // return output;
    return "<div>" + text + "</div>";
  },
  htmlDecodeByRegExp: function (str) {
    var s = "";
    if (str.length == 0) return "";
    s = str.replace(/&amp;/g, "&");
    s = s.replace(/&lt;/g, "<");
    s = s.replace(/&gt;/g, ">");
    s = s.replace(/&nbsp;/g, " ");
    s = s.replace(/%26nbsp;/g, " ");
    s = s.replace(/&#39;/g, "'");
    s = s.replace(/&quot;/g, '"');
    return s;
  },

  shopType: function (v) {
    if (v == 1) return "即时购";
    return "预定";
  },

  StaffType: function (d) {
    var t;
    switch (d) {
      case 1:
        t = "超级管理员";
        break;
      case 2:
        t = "店长";
        break;
      case 3:
        t = "副店长";
        break;
      case 4:
        t = "店员";
        break;
      case 5:
        t = "普通用户";
        break;
      case 6:
        t = "共享配送员";
        break;
      case 9:
        t = "后台登录";
        break;
      case 10:
        t = "后台用户";
        break;
      case 11:
        t = "没有门店";
        break;
      default:
        t = "未知级别";
        break;
    }
    return t;
  },
  cutdate: function (s) {
    return s.substring(0, s.length - 2);
  },
  getAge: function (s) {
    var r = s.match(/^(\d{1,4})(-|\/)(\d{1,2})\2(\d{1,2})$/);
    if (r == null) return false;
    var d = new Date(r[1], r[3] - 1, r[4]);
    if (
      d.getFullYear() == r[1] &&
      d.getMonth() + 1 == r[3] &&
      d.getDate() == r[4]
    ) {
      var Y = new Date().getFullYear();
      return Y - r[1];
    }
    return "输入的日期格式错误！";
  },
  cutfront: function (s, l) {
    return s.substring(l, s.length);
  },
  cutback: function (s, l) {
    return s.substring(0, s.length - l);
  },
  parse: function (s) {
    var o;
    try {
      o = JSON.parse(s);
    } catch (e) {
      o = s;
    }
    return o;
  },
  deData: function (s) {
    if (s && s.indexOf("/") > -1) {
      s = decodeURIComponent(s.replace(/\//g, "%"));
    }
    return Comm.parse(s);
  },
  enData: function (o) {
    return encodeURIComponent(JSON.stringify(o)).replace(/\%/g, "/");
  },
  db: function (t, v) {
    if (v == null) {
      if (arguments.length === 1) {
        return Comm.deData(Comm.Cookie.get(t));
      } else {
        Comm.Cookie.set(t, "");
      }
    } else {
      Comm.Cookie.set(t, Comm.enData(v));
    }
  },
  //调用方法Comm.OSS.getImgUrl(uri,'l');图片前缀更新方法
  OSS: {
    /*阿里云oss工具*/
    host: function () {
      return window["config"] && window.config["ossroot"] ? config.ossroot : "";
    },
    inithtml: function (d, cb) {
      top.Comm.laytpl(top.$("#upimgTpl").html()).render(d, function (html) {
        cb && cb(html);
      });
    },
    showimg: function (a) {
      Comm.layer.open({
        type: 1,
        title: "查看图片",
        maxmin: true,
        area: ["500px", "550px"],
        shade: 0.3,
        content: '<div style="text-align: center;"><img src="' +
          $(a).attr("src") +
          '" width="100%"/></div>',
      });
    },
    /*oss访问地址*/
    /*
        获取图片访问地址
        uri:数据库中保存的文件地址
        type:显示类型 	取值:s|m|l
         */
    getImgUrl: function (uri, type) {
      if (uri == null) return "-----------error";
      if (uri.length >= 4 && uri.indexOf("http") > -1) return uri;
      var url = Comm.OSS.host() + uri;
      if (type) {
        switch (type) {
          case "l":
            url += "/800";
            break;
          case "f":
            url += "/400";
            break;
          case "m":
            url += "/250";
            break;
          case "s":
            url += "/120";
            break;
          default:
            break;
        }
      }
      return url;
    },
  },

  orderType: function (value) {
    switch (Number(value)) {
      case 1:
        return "配送";
      case 2:
        return "自提";
      default:
        return "未知类型";
    }
  },

  warehouseOrderStatus: function (status) {
    var s;
    switch (status) {
      case 1:
        s = "待付款";
        break;
      case 10:
        s = "已支付";
        break;
      case 20:
        s = "备货中";
        break;
      case 30:
        s = "备货中";
        break;
      case 40:
        s = "配送中";
        break;
      case 45:
        s = "已送达";
        break;
      case 50:
        s = "已完成";
        break;
      case 60:
        s = "已取消";
        break;
      case 99:
        s = "已删除";
        break;
      default:
        s = "未知类型";
        break;
    }
    return s;
  },
  afterOrderState: function (d) {
    var s;
    switch (d) {
      case 1:
        s = "待审核";
        break;
      case 2:
        s = "待验货";
        break;
      case 4:
        s = "拒绝申请";
        break;
      case 8:
        s = "通过申请";
        break;
      default:
        s = "未知类型";
        break;
    }
    return s;
  },
  warehouseStatusSelect: function () {
    var str = "<option value=''>请选择</option>";
    str += "<option value='1'>待付款</option>";
    str += "<option value='10'>已支付,待打印</option>";
    // str += "<option value='20'>备货中</option>";
    str += "<option value='30'>备货中</option>";
    str += "<option value='40'>配送中</option>";
    str += "<option value='45'>已送达</option>";
    str += "<option value='50'>已完成</option>";
    str += "<option value='60'>已取消</option>";
    str += "<option value='99'>已删除</option>";
    return str;
  },


  //创建对象，并追加到f里面，attr:对象属性
  create: function (att, f, tag) {
    var a = document.createElement(tag ? tag : "div");
    if (att)
      for (var i in att) a[i] = att[i];
    (Comm.g(f) || document.body).appendChild(a);
    return a;
  },
  g: function (o) {
    if (typeof o == typeof {}) return o;
    return document.getElementById(o);
  },
  //尝试执行根方法
  exec: function (m) {
    if (window[m]) {
      var a = [];
      for (var i = 1; i < arguments.length; i++) a[i - 1] = arguments[i];
      window[m].apply(null, a);
    }
  },
  //显示加载s为需要显示的内容,s为false则表示关闭
  loading: function (s) {
    if (s) {
      Comm.layer.load();
    } else {
      Comm.layer.closeAll("loading");
    }
  },
  msg: function (msg, type) {
    Comm.layer.msg(msg, {
      icon: type,
    });
  },
  alert: function (str) {
    Comm.layer.alert(str, {
      icon: 1,
    });
  },
  //confirm cb返回true,false
  confirm: function (str, cb) {
    Comm.layer.confirm(
      str, {
      icon: 3,
      title: "提示",
    },
      function (index) {
        cb && cb(1);
        layer.close(index);
      }
    );
  },
  //查询url参数，n:名称
  query: function (n, u) {
    var s = u;
    if (s == null) s = self.location.href;
    if (n) {
      var g = new RegExp("(\\?|&)" + n + "=([^&|#]*)");
      var r = s.match(g);
      if (r) {
        try {
          return decodeURIComponent(r[2]);
        } catch (err) {
          return unescape(r[2]);
        }
      } else return "";
    } else {
      var i = s.indexOf("?");
      if (i === -1) return "";
      return s.substr(i + 1);
    }
  },
  go: function (r) {
    location.href = r;
  },
  price: function (v) {
    if (v == null || v == 0 || typeof v == "undefined") {
      return 0;
    }

    if (v != null && v != "" && v != undefined) {
      return Number(v / 100).toFixed(2);
    }
  },
  enPrice: function (v) {
    if (v == null || v == 0 || typeof v == "undefined") {
      return 0;
    }

    if (v != null && v != "" && v != undefined) {
      return Number(v * 100).toFixed(0);
    }
  },

  enableStr: function (index, type) {
    var enable = ["启用", "上架"];
    var disable = ["禁用", "下架"];
    if (type == 1) {
      return '<span style="color: limegreen">' + enable[index] + "</span>";
    } else if (type == 0) {
      return '<span style="color: red">' + disable[index] + "</span>";
    }
  },

  dateTimes: function (stamp) {
    return stamp;
    if (stamp == undefined) {
      return "";
    }
    var date = new Date(stamp + 8 * 3600 * 1000); // 增加8小时
    //	var date = new Date(da);
    if (date.toJSON() == null || date.toJSON().length == 0 || stamp == 0)
      return "";
    return date.toJSON().substr(0, 19).replace("T", " ");
  },

  checkImg: function (key) {
    var keys = [
      "user_share_img",
      "coupons_share_img",
      "coupons_chat_img",
      "newbie_coupons_background",
      "invite_friend_img",
      "invite_friendsOut_img",
      "register_now_img",
      "active_back_img",
      "service_idea_img",
    ];
    for (var i in keys) {
      if (key == keys[i]) {
        return true;
      }
    }
    return false;
  },

  logActionType: function (a) {
    if (a == "goodsUp") {
      return "上架";
    } else if (a == "goodsDown") {
      return "下架";
    } else if (a == "goodsUpdate") {
      return "修改商品基本信息";
    }
  },

  GetWeekData: function (data, name) {
    var arr = [0, 0, 0, 0, 0, 0, 0];

    for (j in data) {
      switch (name) {
        case "reg":
          arr[j] = data[j].reg;
          break;
        case "acc":
          arr[j] = data[j].acc;
          break;
        case "sale":
          arr[j] = data[j].sale;
          break;
        case "sales":
          arr[j] = Comm.price(data[j].sales);
          break;
      }
    }

    return arr;
  },
  subYYYY: function (d) {
    return d.substring(5);
  },

  //时间转换函数
  format: function (stamp, type) {
    type = type && type.match(/\w+/g);
    var now = new Date(stamp),
      year = now.getFullYear(),
      month = now.getMonth() + 1,
      date = now.getDate(),
      hour = now.getHours(),
      minute = now.getMinutes(),
      second = now.getSeconds(),
      o = [],
      arr = {
        yyyy: year,
        MM: check(month),
        dd: check(date),
        h: check(hour),
        m: check(minute),
        s: check(second),
      };
    if (type) {
      for (var i = 0; i < type.length; i++) {
        var tmp = type[i];
        tmp = tmp.toLocaleLowerCase();
        if (arr[type[i]])
          o.push(type[i] == "h" ? " " + arr[type[i]] : "-" + arr[type[i]]);
      }
      if (o.length) return o.join("").slice(1);
    } else
      return (
        arr.yyyy +
        "-" +
        arr.MM +
        "-" +
        arr.dd +
        " " +
        arr.h +
        ":" +
        arr.m +
        ":" +
        arr.s
      );

    function check(num) {
      if (num < 10) {
        return "0" + num;
      }
      return num;
    }
  },
  wait: 60,
  timeCountDownclick: true,
  timeCountDown: function (o, phone, type, imgcode) {
    /*发送验证码公用方法*/
    /*o 点击发送验证码按钮*/
    /*phone 发送短信手机号*/
    /*type 验证码类型*/
    /*imgcode 图片验证码，可不传*/
    /*调用 app.timeCountDown(this,15928877394,1,5421)*/
    //按钮倒计时
    imgcode = imgcode == undefined ? "" : imgcode;
    if (!phone) {
      layer.msg("请输入手机号");
      return;
    }
    var reg = /^1\d{10}$/;
    if (phone && !reg.test(phone)) {
      layer.msg("手机格式错误");
      return;
    }
    if (Comm.timeCountDownclick) {
      Comm.timeCountDownclick = false;
      o.setAttribute("disabled", true);
      AJAX.GET(
        "/api/customer/sendSMS?phone=" +
        phone +
        "&type=" +
        type +
        "&_device=" +
        1,
        function (d) {
          if (d.code == 1) {
            layer.msg("短信已发送，请注意查收");
            var i = setInterval(function () {
              if (Comm.wait == 0) {
                o.removeAttribute("disabled");
                o.value = "重新发送";
                Comm.wait = 60;
                clearInterval(i);
              } else {
                o.value = Comm.wait + "秒后重发";
                Comm.wait--;
              }
            }, 1000);
          } else {
            o.removeAttribute("disabled");
            layer.msg(d.msg);
          }
          Comm.timeCountDownclick = true;
        }
      );
    }
  },

  checkPhone: function () {
    var b = true;
    var myreg = /^[1][3,4,5,7,8,9][0-9]{9}$/;
    for (var index in arguments) {
      if (!myreg.test(arguments[index])) {
        Comm.msg("请输入正确的电话", 5);
        b = false;
        break;
      }
    }
    if (b == false) return b;
  },

  checkPhoneByParam: function (phone, msg) {
    var myreg = /^[1][3,4,5,7,8,9][0-9]{9}$/;
    if (!myreg.test(phone)) {
      Comm.msg(msg, 5);
      return true;
    }
  },

  html2Escape: function (sHtml) {
    return sHtml.replace(/[<>&"]/g, function (c) {
      return {
        "<": "&lt;",
        ">": "&gt;",
        "&": "&amp;",
        '"': "&quot;",
      }[c];
    });
  },
  getLineOption: function (name, xdata, data, ds) {
    var option = {
      grid: {
        top: "5%",
        right: "1%",
        left: "1%",
        bottom: "5%",
        containLabel: true,
      },

      tooltip: {
        trigger: "axis",
        formatter: "{b0}<br>{a0}:{c0}" + ds,
      },
      xAxis: {
        type: "category",
        boundaryGap: false,
        axisLabel: {
          interval: 0,
          rotate: 40,
        },
        data: xdata, //
      },
      yAxis: {
        type: "value",
      },
      series: [{
        name: name,
        data: data,
        type: "line",
        smooth: true,
        areaStyle: {},
        itemStyle: {
          normal: {
            label: {
              show: true,
            },
          },
        },
      },],
    };
    return option;
  },

  radiusOption: function (dataname, ds, data) {
    // 指定图表的配置项和数据
    var option = {
      tooltip: {
        trigger: "item",
        formatter: "{a} <br/>{b} : {c} ({d}%)",
      },
      legend: {
        orient: "vertical",
        left: "left",
        data: dataname,
      },
      series: [{
        name: ds,
        type: "pie",
        radius: "55%",
        center: ["50%", "60%"],
        data: data,
        itemStyle: {
          emphasis: {
            shadowBlur: 10,
            shadowOffsetX: 0,
            shadowColor: "rgba(0, 0, 0, 0.5)",
          },
          normal: {
            label: {
              show: true,
              formatter: "{b} : {c} ({d}%)",
            },
            labelLine: {
              show: true,
            },
          },
        },
      },],
    };
    return option;
  },
  GetDatas: function (id) {
    var p = "";
    var id = id == undefined || id == "" ? "form" : id;
    $("#" + id)
      .find("input,textarea,select")
      .each(function (i, e) {
        if ($(e).attr("name") != undefined) {
          if ($(e).attr("laydate") != undefined) {
            p = p + "&" + $(e).attr("name") + "=" + $(e).val();
          } else {
            p = p + "&" + $(e).attr("name") + "=" + $(e).val();
          }
        }
      });
    return p;
  },
  GetData: function (id) {
    var p = {};
    var id = id == undefined || id == "" ? "form" : id;
    $("#" + id)
      .find("input,textarea,select")
      .each(function (i, e) {
        if ($(e).attr("name") != undefined) {
          if ($(e).attr("laydate") != undefined) {
            p[$(e).attr("name")] = $(e).val();
          } else {
            p[$(e).attr("name")] = $(e).val();
          }
        }
      });
    return p;
  },
  SetData: function (p, id) {
    var id = id == undefined || id == "" ? "form" : id;

    $("#" + id)
      .find("input,textarea,select")
      .each(function (i, e) {
        if ($(e).attr("name") != undefined && $(e).attr("type") != "radio") {
          var v = p[$(e).attr("name")];
          if ((void 0 !== v && null !== v && "null" !== v) || (v = "")) {
            $(e).val(v);
            if (e.hasAttribute("laydate")) {
              Comm.laydateinit(e);
            }
          }
        }
      });
    return p;
  },
  laydateinit: function (e) {
    var c = {
      elem: "#" + $(e).attr("id"),
      type: $(e).attr("type") != undefined && $(e).attr("type") != "" ?
        $(e).attr("type") : "date",
      range: $(e).attr("range") != undefined ? true : false,
    };
    if (e.hasAttribute("min")) {
      c.min = $(e).attr("min");
    }
    if (e.hasAttribute("max")) {
      c.max = $(e).attr("max");
    }
    if (e.hasAttribute("format")) {
      c.format = $(e).attr("format");
    }
    if ($(e).val() != "") {
      c.value = $(e).val();
    }
    $(e).attr("type", "text");
    Comm.laydate.render(c);
  },

  dateFormat: function (fmt, date) {
    let ret;
    const opt = {
      "y+": date.getFullYear().toString(), // 年
      "M+": (date.getMonth() + 1).toString(), // 月
      "d+": date.getDate().toString(), // 日
      "h+": date.getHours().toString(), // 时
      "m+": date.getMinutes().toString(), // 分
      "s+": date.getSeconds().toString(), // 秒
    };
    for (let k in opt) {
      ret = new RegExp("(" + k + ")").exec(fmt);
      if (ret) {
        fmt = fmt.replace(
          ret[1],
          ret[1].length == 1 ? opt[k] : opt[k].padStart(ret[1].length, "0")
        );
      }
    }
    return fmt;
  },

  uploadImg: function (btnId, divId, imgId, imgSize, nowLength) {
    if (imgSize == null) {
      imgSize = "max-width: 200px;max-height: 200px;";
    }
    var sfaceNum = nowLength;
    Comm.upload.render({
      elem: "#" + btnId, //绑定元素
      field: "file",
      accept: "images",
      acceptMime: "image/*",
      multiple: false, //单图
      url: config.ossroot,
      size: 300,
      before: function (obj, o) {
        layer.load();
      },
      done: function (d) {
        sfaceNum++;
        layer.closeAll("loading"); //关闭loading
        var htmlStr1 = '<div class="layui-inline ">';
        htmlStr1 += '<img class="layui-upload-img" src="' + Comm.OSS.getImgUrl(this.data.key) + '" style="' + imgSize + '" data="' + this.data.key + '"/><br/>';
        htmlStr1 += '<button type="button" class="layui-btn layui-btn-xs layui-btn-danger" onclick="$(this).parent().remove()">删除</button></div>';
        $("#" + imgId).val(this.data.key);
        if (sfaceNum > 1) {
          $("#" + divId).html(htmlStr1);
        } else {
          $("#" + divId).append(htmlStr1);
        }
      },
      error: function () {
        layer.closeAll("loading"); //关闭loading
      },
    });
  },
  uploadImgs: function (btnId, divId, imgId, imgSize, nowLength, maxLength) {
    var iii = nowLength;
    if (imgSize == null) {
      imgSize = "max-width: 200px;max-height: 200px;";
    }
    Comm.upload.render({
      elem: "#" + btnId, //绑定元素
      field: "file",
      accept: "images",
      acceptMime: "image/*",
      multiple: true, //多图
      number: maxLength,
      url: config.ossroot,
      size: 300,
      before: function (obj) { },
      done: function (d) {
        iii++;
        if ($("#" + divId).children().length > maxLength - 1) {
          Comm.msg("最多上传" + maxLength + "张");
        } else {
          layer.closeAll("loading"); //关闭loading
          if (iii == 1) {
            layer.msg("上传成功", {
              time: 2000,
            });
          }
          var htmlStr = '<div class="layui-inline">';
          htmlStr += '<img class="layui-upload-img" src="' + Comm.OSS.getImgUrl(this.data.key, "s") + '" style="' + imgSize + '" data="' + this.data.key + '"/><br/>';
          htmlStr += '<button type="button" class="layui-btn layui-btn-xs layui-btn-danger" onclick="$(this).parent().remove()">删除</button></div>';
          $("#" + imgId).val($("#" + imgId).val() + this.data.key + ",");
          $("#" + divId).append(htmlStr);
        }
      },
      error: function () {
        layer.closeAll("loading"); //关闭loading
        layer.msg("上传失败", {
          time: 2000,
        });
      },
    });
  },
  uploadVideo: function (btnId, divId, videoId, showSize, nowLength) {
    var vSize = nowLength;
    if (showSize == null) {
      showSize = "max-width: 400px;max-height: 300px;";
    }
    Comm.upload.render({
      elem: "#" + btnId,
      field: "file",
      accept: "video",
      acceptMime: "video/*",
      multiple: false,
      number: 1,
      size: 5 * 1024,
      url: config.ossroot,
      before: function (obj) { },
      done: function (d) {
        vSize++;
        var htmlStr = '<div class="layui-inline">';
        htmlStr += '<video class="layui-upload" style="' + showSize + '" ' + 'src="' + Comm.OSS.getImgUrl(this.data.key) + '" controls="controls" data="' + this.data.key + '"/><br/>' + "</video>";
        htmlStr += '<button type="button" class="layui-btn layui-btn-xs layui-btn-danger" onclick="$(this).parent().remove()">删除</button></div>';
        $("#" + videoId).val(this.data.key);
        if (vSize >= 1) {
          $("#" + divId).html(htmlStr);
        } else {
          $("#" + divId).append(htmlStr);
        }
      },
      error: function () {
        layer.closeAll("loading"); //关闭loading
        layer.msg("上传失败", {
          time: 2000,
        });
      },
    });
  },

  showImg: function (data, imgSize, imgId, divId) {
    if (imgSize == null) {
      imgSize = "max-width: 200px;max-height: 200px;";
    }
    var htmlStr1 = '<div class="layui-upload-list">';
    htmlStr1 += '<img class="layui-upload-img" src="' + Comm.OSS.getImgUrl(data) + '" style="' + imgSize + '" data="' + data + '"/><br/>';
    if (imgId != null) {
      htmlStr1 += '<button type="button" class="layui-btn layui-btn-xs layui-btn-danger" onclick="$(this).parent().remove()">删除</button></div>';
      $("#" + imgId).val(data);
    }
    $("#" + divId).append(htmlStr1);
  },

  showImgWithOutDel: function (data, imgSize, imgId, divId) {
    if (imgSize == null) {
      imgSize = "max-width: 200px;max-height: 200px;";
    }
    var htmlStr1 = '<div class="layui-upload-list">';
    htmlStr1 += '<img class="layui-upload-img" src="' + Comm.OSS.getImgUrl(data) + '" style="' + imgSize + '" data="' + data + '"/><br/>';
    if (imgId != null) {
      $("#" + imgId).val(data);
    }
    $("#" + divId).append(htmlStr1);
  },

  delImg: function (key, id) {
    var imgs = $("#" + id).val();
    if (imgs != null && imgs != "") {
      var resImgs = ("," + imgs).replace(",", "," + key + ",");
      resImgs = resImgs.substring(0, resImgs.length);
      $("#" + id).val(resImgs);
    }
  },

  warehouseLoad: function (selectId) {
    var ob = document.getElementById(selectId);
    ob.innerHTML = '<option value="">请选择订货前置仓</option>';
    AJAX.GET('/admin/warehouse/getWarehouseSimpleList', function (d) {
      if (d.code == 1) {
        var data = d.data;
        for (var i in data) {
          var op = document.createElement('option');
          op.setAttribute("value", data[i].warehouseId);
          op.innerText = data[i].warehouseName;
          ob.appendChild(op);
        }
        Comm.form.render();
      } else {
        Comm.msg(d.msg, 5);
      }
    })
  },
};

var $ = null;
window.onload = function () {
  layui.config({
    base: "inc/",
  });
  layui.use(
    [
      "table",
      "laydate",
      "form",
      "laytpl",
      "layer",
      "jquery",
      "upload",
      "element",
    ],
    function () {
      //初始化,全局变量 不可污染
      (Comm.table = layui.table),
        (Comm.laytpl = layui.laytpl),
        (Comm.form = layui.form),
        (Comm.laydate = layui.laydate),
        (Comm.layer = layui.layer),
        ($ = layui.jquery),
        (Comm.upload = layui.upload);

      /*处理时间选择器*/
      $("input[laydate]").each(function (i, e) {
        Comm.laydateinit(e);
      });

      Comm.exec("pageload");

      //处理页面table
      /*search*/
      Comm.search = function () {
        //执行重载
        Comm.form.render();
        Comm.table.reload("table", {
          page: {
            curr: 1,
          }, //重新从第 1 页开始
          where: Comm.GetData("search-form"),
        });
      };
      /*sort*/
      Comm.table.on("sort(table)", function (obj) {
        var p = Comm.GetData("search-form");
        p[obj.field] = obj.type;
        Comm.table.reload("table", {
          page: {
            curr: 1,
          }, //重新从第 1 页开始
          initSort: obj,
          where: p,
        });
      });

      /*--------------------------------------------------------------------------------------------------------*/
      //解决行工具多的时候，显示后点击没效果问题
      $(document)
        .off("mousedown", ".layui-table-grid-down")
        .on("mousedown", ".layui-table-grid-down", function (event) {
          _tableTrCurr = $(this).closest("td");
        });

      $(document)
        .off("click", ".layui-table-tips-main [lay-event]")
        .on("click", ".layui-table-tips-main [lay-event]", function (event) {
          var elem = $(this);
          var tableTrCurr = _tableTrCurr;
          if (!tableTrCurr) {
            return;
          }
          var layerIndex = elem.closest(".layui-table-tips").attr("times");
          // 关闭当前这个显示更多的tip
          layer.close(layerIndex);
          _tableTrCurr
            .find('[lay-event="' + elem.attr("lay-event") + '"]')
            .first()
            .click();
        });
      /*---------------------------------------------------------------------------------------------------*/
    }
  );
};
