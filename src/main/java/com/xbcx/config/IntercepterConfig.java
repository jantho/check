//package com.xbcx.config;
//
//import com.xbcx.util.TokenInterceptor;
//import org.springframework.context.annotation.Configuration;
//import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
//import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
//
//import java.util.ArrayList;
//import java.util.List;
//
///**
// * @Auther WuBingQing
// * @Date 2021/3/29
// */
//@Configuration
//public class IntercepterConfig implements WebMvcConfigurer {
//
//    private TokenInterceptor tokenInterceptor;
//
//    //构造方法
//    public IntercepterConfig(TokenInterceptor tokenInterceptor) {
//        this.tokenInterceptor = tokenInterceptor;
//    }
//
//    @Override
//    public void addInterceptors(InterceptorRegistry registry) {
//
//        List<String> excludePath = new ArrayList<>();
//        excludePath.add("/User/login"); //登录
//
//        registry.addInterceptor(tokenInterceptor)
//                .addPathPatterns("/**")
//                .excludePathPatterns(excludePath);
//        WebMvcConfigurer.super.addInterceptors(registry);
//    }
//}