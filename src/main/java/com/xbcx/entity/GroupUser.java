package com.xbcx.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Date;


/**
 * @Author: 伍炳清
 * @Date: 2021/3/23 11:53
 */
@Data
@NoArgsConstructor
@AllArgsConstructor

//定义表名
@TableName(value = "t_groups_user")
public class GroupUser extends Model<GroupUser> implements Serializable {

    private static final long serialVersionUID = 15L;
    /**
     * 考勤组用户关联主键
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
     * 考勤组id
     */
    @TableField("group_id")
    private Integer groupId;
    /**
     * 用户id
     */
    @TableField("user_id")
    private Integer userId;


}
