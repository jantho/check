package com.xbcx.util;

import sun.misc.BASE64Encoder;

import java.security.MessageDigest;
import java.util.UUID;

public class Md5Utils {

	/**
	 * 获取盐
	 * @return
	 */
	public static String createSalt() {
		//生成全球唯一的id
		UUID uuid = UUID.randomUUID();
		return uuid.toString();
	}

	/**
	 * 加密
	 * @param password
	 * @return
	 */
	public static String md5Password(String password,String salt) {

		try {
			// 1.获取MD5加密算法
			MessageDigest digest = MessageDigest.getInstance("md5");
			// 2.进行加密
			byte[] md5 = digest.digest((password+salt).getBytes());

			// 3.编码
			BASE64Encoder encoder = new BASE64Encoder();
			return encoder.encode(md5);

		} catch (Exception e) {
			e.printStackTrace();
			throw new RuntimeException("加密失败!");
		}

	}

	

}
