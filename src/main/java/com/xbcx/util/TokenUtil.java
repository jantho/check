package com.xbcx.util;

import java.util.HashMap;
import java.util.Map;

/**
 * @Auther WuBingQing
 * @Date 2021/3/26
 */
public class TokenUtil {

    private static String key = "wuBingQing";

    private static String salt = "haHaHa";


    //token生成
    public static String getToken(Integer id, String username) {

        //分别对id和username
        String sId = AESUtil.jaAES(id.toString(), key);
        String sUsername = AESUtil.jaAES(username.toString(), key);
        //拼接成token
        StringBuilder token = new StringBuilder();
        token.append(sId.substring(0, 22));
        token.append(",");
        token.append(sUsername.substring(0, 22));
        return token.toString();
    }

    //token解析
    public static Map readToken(String token) {
        String[] tokens = token.split(",");
        HashMap<String, Object> idAndUsername = new HashMap<>();

        if (tokens.length == 2) {
            String id = AESUtil.jieAES(tokens[0], key);
            String username = AESUtil.jieAES(tokens[1], key);
            idAndUsername.put("id", id);
            idAndUsername.put("username", username);
        }
        return idAndUsername;
    }
}
