package com.xbcx.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.xbcx.entity.User;
import com.xbcx.model.UserListDTO;
import com.xbcx.model.UserListVO;
import org.apache.ibatis.annotations.Param;

/**
 * @author 伍炳清
 * @date 2020-10-15 20:30
 */
//接口只需继承Mapper
public interface UserMapper extends BaseMapper<User> {
    /**
     * 分页查询用户列表
     *
     * @param page
     * @return
     */
    Page<UserListVO> getUserList(Page<UserListVO> page,@Param("dto") UserListDTO dto);

}
