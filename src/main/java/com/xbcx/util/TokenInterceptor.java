//package com.xbcx.util;
//
//import com.xbcx.entity.User;
//import com.xbcx.mapper.UserMapper;
//import org.springframework.stereotype.Component;
//import org.springframework.web.servlet.HandlerInterceptor;
//
//import javax.annotation.Resource;
//import javax.servlet.http.HttpServletRequest;
//import javax.servlet.http.HttpServletResponse;
//import javax.servlet.http.HttpSession;
//import java.io.PrintWriter;
//import java.util.Map;
//
///**
// * @Auther WuBingQing
// * @Date 2021/3/29
// */
//@Component
//public class TokenInterceptor implements HandlerInterceptor {
//
//    @Resource
//    private UserMapper userMapper;
//
//    @Override
//    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
//
//        if (request.getMethod().equals("OPTIONS")) {
//            response.setStatus(HttpServletResponse.SC_OK);
//            return true;
//        }
//        response.setCharacterEncoding("utf-8");
//        String token = request.getHeader("admin-token");
//        //System.out.println("获取到的token：" + token);
//        if (token != null && !(token.equals("undefined"))) {
//            Map token1 = TokenUtil.readToken(token);
//            //认证
//            User user = userMapper.selectById(Integer.valueOf(token1.get("id").toString()));
//            //System.out.println("登录用户：" + user);
//            //System.out.println(token1.get("username"));
//            if (user.getUsername().equals(token1.get("username"))) {
//                System.out.println("通过拦截器");
//                return true;
//            }
//        }
//        response.setCharacterEncoding("UTF-8");
//        response.setContentType("application/json; charset=utf-8");
//        PrintWriter out = null;
//        try {
//            System.out.println("认证失败，未通过拦截器");
//            response.getWriter().append("token认证失败");
//            //-----------------------------------------
//            response.sendRedirect("login");
//            System.out.println("认证失败，未通过拦截器2");
//
//            //-----------------------------------------
//        } catch (Exception e) {
//            e.printStackTrace();
//            response.sendError(500);
//            return false;
//        }
//        return false;
//    }
//
//}