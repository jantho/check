import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.xbcx.MainController;
import com.xbcx.entity.Classes;
import com.xbcx.entity.User;
import com.xbcx.mapper.ClassesMapper;
import com.xbcx.mapper.UserMapper;
import com.xbcx.model.UserListDTO;
import com.xbcx.model.UserListVO;
import com.xbcx.service.UserService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.Resource;
import java.util.Date;

/**
 * @Author: 伍炳清
 * @Date: 2021/3/23 10:25
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = MainController.class)
public class TestClassesMapper {

    @Resource
    private ClassesMapper classesMapper;


    @Test
    public void addclasses() {
        Date date = new Date();
        Classes classes = new Classes(null, 2, date, date, 2, 2, date,  0,1);
        classesMapper.insert(classes);

    }

    @Test
    public void Test() {
        int createTime = (int) new Date().getTime();
        System.out.println(new Date().getTime());
        System.out.println("转int:" + createTime);
        System.out.println("转回:" + Long.valueOf(createTime));


    }


}
