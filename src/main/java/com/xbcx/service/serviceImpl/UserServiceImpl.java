package com.xbcx.service.serviceImpl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.xbcx.exception.CustomerException;
import com.xbcx.mapper.UserMapper;
import com.xbcx.entity.User;
import com.xbcx.model.UserListDTO;
import com.xbcx.model.UserListVO;
import com.xbcx.service.UserService;
import com.xbcx.util.Md5Utils;
import org.springframework.stereotype.Service;


import javax.annotation.Resource;
import java.util.Date;
import java.util.List;

/**
 * @author 伍炳清
 * @date 2021-10-16 14:50
 */

@Service("userService")
public class UserServiceImpl implements UserService {

    @Resource
    private UserMapper userMapper;


    @Override
    public void addUser(User user) throws CustomerException {
        //判空
        if (user.getAccount() == null || user.getAccount().equals("")) {
            throw new CustomerException("账户不能为空！");
        }
        if (user.getPwd() == null || user.getPwd().equals("")) {
            throw new CustomerException("密码不能为空！");
        }
        if (user.getRoleId() == null) {
            throw new CustomerException("角色id不能为空！");
        }
        if (user.getDeptId() == null) {
            throw new CustomerException("部门id不能为空！");
        }
        if (user.getComId() == null) {
            throw new CustomerException("公司id不能为空！");
        }
        if (user.getUid() == null) {
            throw new CustomerException("创建人不能为空！");
        }
        //查询账户是否重复
        User user1 = userMapper.selectOne(new LambdaQueryWrapper<User>()
                .eq(User::getAccount, user.getAccount())
                .last("limit 1"));
        if (user1 != null) {
            throw new CustomerException("账户已存在！");
        }
        //加密密码
        String md5Password = Md5Utils.md5Password(user.getPwd(), "Md5JiaMi");
        user.setPwd(md5Password);
        //创建时间,更新时间,初始化未删除状态为0
        user.setCreateTime(new Date());
        user.setUpdateTime(new Date());
        user.setIsDel(0);
        //新增
        userMapper.insert(user);
    }

    @Override
    public void updateUser(User user) throws CustomerException {
        //判空
        if (user.getRoleId() == null) {
            throw new CustomerException("角色id不能为空！");
        }
        if (user.getDeptId() == null) {
            throw new CustomerException("部门id不能为空！");
        }
        if (user.getComId() == null) {
            throw new CustomerException("公司id不能为空！");
        }
        //设置更新时间
        user.setUpdateTime(new Date());
        //更新
        userMapper.updateById(user);


    }

    @Override
    public User login(String account, String pwd) throws CustomerException {
        //判空
        if (account == null || account.equals("")) {
            throw new CustomerException("账户不能为空！");
        }
        if (pwd == null || pwd.equals("")) {
            throw new CustomerException("密码不能为空！");
        }
        //加密密码
        String md5Password = Md5Utils.md5Password(pwd, "Md5JiaMi");
        //查询
        User user = userMapper.selectOne(new LambdaQueryWrapper<User>()
                .eq(User::getAccount, account)
                .eq(User::getPwd, md5Password)
                .last("limit 1"));
        if (user == null) {
            throw new CustomerException("用户名或密码错误");
        }
        if (user.getIsDel() == 1) {
            throw new CustomerException("账户不存在！");
        }

        return user;

    }

    @Override
    public Page<UserListVO> pageUser(UserListDTO dto) throws CustomerException {
        //判空
        if (dto.getPageNo() == null) {
            throw new CustomerException("当前页数不能为空！");
        }
        if (dto.getPageSize() == null) {
            throw new CustomerException("分页大小不能为空！");
        }
        Page<UserListVO> page = new Page<>(dto.getPageNo(), dto.getPageSize());
        Page<UserListVO> userList = userMapper.getUserList(page, new UserListDTO());
        return page;
    }

}

