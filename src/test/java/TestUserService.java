import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.xbcx.MainController;
import com.xbcx.entity.User;
import com.xbcx.exception.CustomerException;
import com.xbcx.mapper.UserMapper;
import com.xbcx.model.UserListDTO;
import com.xbcx.model.UserListVO;
import com.xbcx.service.UserService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.Resource;
import java.util.Date;

/**
 * @Author: 伍炳清
 * @Date: 2021/3/23 10:25
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = MainController.class)
public class TestUserService {

    @Resource
    private UserService userService;

    @Test
    public void addUser() {
        User user = new User(null, "小明", "xioaming",
                "123456", "123456", 1, 1, 1,
                1, null, null, null);

        try {
            userService.addUser(user);
        } catch (CustomerException e) {
            System.out.println(e.getMessage());
        }
    }

    @Test
    public void updateUser() {
        User user = new User(2, "李狗蛋改", "ligoudangai",
                null, "222", 2, 2, 2,
                1, null, null, 1);

        try {
            userService.updateUser(user);
        } catch (CustomerException e) {
            System.out.println(e.getMessage());
        }
    }

    @Test
    public void page() {
        UserListDTO userListDTO = new UserListDTO();
        userListDTO.setPageNo(1);
        userListDTO.setPageSize(3);
        try {
            Page<UserListVO> page = userService.pageUser(userListDTO);
            System.out.println(page.getRecords());
        } catch (CustomerException e) {
            System.out.println(e.getMessage());
        }


    }


    @Test
    public void Test() {
        int createTime = (int) new Date().getTime();
        System.out.println(new Date().getTime());
        System.out.println("转int:" + createTime);
        System.out.println("转回:" + Long.valueOf(createTime));


    }


}
